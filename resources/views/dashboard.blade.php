@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="row">
            <div class="col-xl-2">
                <div class="card mb-4">
                    <a href="#" class="btn btn-success" role="button" aria-pressed="true">Add</a>
                </div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Product list
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Category</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                            <td>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-primary btn-block" role="button" aria-pressed="true">Edit</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-danger btn-block" role="button" aria-pressed="true">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection
