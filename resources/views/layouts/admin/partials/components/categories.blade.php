@php
    use App\Models\Admin\Category;
    
    $categories = Category::where('parent_id', NULL)
        ->with('children')
        ->where('status', 'ACTIVE')->get();
@endphp

<a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
    Categories
    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
</a>
<div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
        <a class="nav-link collapsed" href="{{route('categories')}}">
            All Categories
        </a>
    </nav>
    @if($categories->count() > 0)
        @foreach($categories as $category)
            @if($category->children()->count() > 0)
                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages2">
                    <a class="nav-link collapsed" href="{{url('/product/category/' . $category->slug)}}" data-bs-toggle="collapse" data-bs-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                        {{$category->name}}
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                        <nav class="sb-sidenav-menu-nested nav">
                            @foreach($category->children()->get() as $categoryChildren)
                                <a class="nav-link" href="{{url('/product/category/' . $categoryChildren->slug)}}">{{$categoryChildren->name}}</a>
                            @endforeach
                        </nav>
                    </div>
                </nav>
            @else
                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages3">
                    <a class="nav-link collapsed" href="{{url('/product/category/' . $category->slug)}}">
                        {{$category->name}}
                    </a>
                </nav>
            @endif
        @endforeach
    @endif
</div>