@extends('layouts.master')

@section('content')
<div id="content" class="site-content">
   <div id="primary" class="content-area width-normal">
      <main id="main" class="site-main">
         <div class="cont maincont">
            <article class="page-cont">
               <div class="page-styling">
                  <div class="auth-wrap">
                     <div class="auth-col">
                        <h2>Авторизация</h2>
                        <form method="post" class="login" action="{{ route('login') }}">
                            @csrf
                           <p>
                              <label for="username">E-mail <span class="required">*</span></label>
                              <input type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="username">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           </p>
                           <p>
                              <label for="password">Пароль <span class="required">*</span></label>
                              <input type="password" name="password" required autocomplete="current-password" id="password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           </p>
                           <p class="auth-submit">
                              <input type="submit" value="Войти">
                              <input type="checkbox" id="rememberme" name="remember" {{ old('remember') ? 'checked' : '' }} value="forever">
                              
                              <label for="rememberme">Запомните меня</label>
                           </p>
                           @if (Route::has('password.request'))
                                <p class="auth-lost_password">
                                   <a href="{{ route('password.request') }}">Забыли пароль?</a>
                                </p>
                           @endif
                        </form>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </main>
      <!-- #main -->
   </div>
   <!-- #primary -->    
</div>
<!-- #content -->
@endsection
