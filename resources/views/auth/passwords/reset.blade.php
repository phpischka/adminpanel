@extends('layouts.master')

@section('content')
<div id="content" class="site-content">
   <div id="primary" class="content-area width-normal">
      <main id="main" class="site-main">
         <div class="cont maincont">
            <article class="page-cont">
               <div class="page-styling">
                  <div class="auth-wrap">
                     <div class="auth-col">
                        <h2>Обновление пароля</h2>
                        
                        <form method="post" action="{{ route('password.update') }}" class="register">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                           <p>
                              <label for="reg_email">Email <span class="required">*</span></label>
                              <input type="email" name="email" value="{{ $email ?? old('email2') }}" required autocomplete="email" id="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           </p>
                           <p>
                              <label for="reg_password">Пароль <span class="required">*</span></label>
                              <input type="password" name="password" required autocomplete="new-password" id="reg_password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           </p>
                           <p>
                              <label for="reg_password">Подтвердить пароль <span class="required">*</span></label>
                              <input type="password" name="password_confirmation" required autocomplete="new-password" id="reg_password">
                           </p>
                           <p class="auth-submit">
                              <input type="submit" value="Обновить пароль">
                           </p>
                        </form>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </main>
      <!-- #main -->
   </div>
   <!-- #primary -->    
</div>
<!-- #content -->
@endsection
