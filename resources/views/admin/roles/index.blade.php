@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4 mb-4">Roles</h1>

        <div class="row">
            <div class="col-xl-2">
                <div class="card mb-4">
                    <a href="{{route('create_role')}}" class="btn btn-success" role="button" aria-pressed="true">Add</a>
                </div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Roles list
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role->id}}</td>
                                 <td>{{$role->name}}</td>
                                <td>{{$role->slug}}</td>
                                <td> 
                                    <div class="col-md-12">
                                        <a href="{{url('admin/roles/' . $role->id . '/edit')}}" class="btn btn-primary btn-block mb-1 btn-sm" role="button" aria-pressed="true">Edit</a>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="{{url('admin/roles/' . $role->id . '/delete')}}" class="btn btn-danger btn-block btn-sm" role="button" aria-pressed="true">Delete</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection
