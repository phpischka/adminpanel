@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4 mb-4">Products</h1>

        <div class="row">
            <div class="col-xl-2">
                <div class="card mb-4">
                    <a href="{{route('create_product')}}" class="btn btn-success" role="button" aria-pressed="true">Add</a>
                </div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Product list
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Category</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Phone</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->category->name ?? ''}}</td>
                                <td>{{$product->title}}</td>
                                <td>{{$product->description}}</td>

                                <td>
                                    @if($product->image)
                                        <img src="{{url('storage/' . $product->image)}}" width="100" alt="image"/>
                                    @endif
                                </td>

                                <td>{{$product->phone}}</td>
                                <td>
                                    <div class="col-md-12">
                                        <a href="{{url('product/' . $product->slug)}}" class="btn btn-warning btn-block mb-1 btn-sm" role="button" aria-pressed="true">Show</a>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="{{url('admin/products/' . $product->id . '/edit')}}" class="btn btn-primary btn-block mb-1 btn-sm" role="button" aria-pressed="true">Edit</a>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="{{url('admin/products/' . $product->id . '/delete')}}" class="btn btn-danger btn-block btn-sm" role="button" aria-pressed="true">Delete</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection
