@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4 mb-4">Edit Product</h1>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Product
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{url('admin/products/' . $product->id . '/update')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-3">
                        <label for="category">Category</label>
                        <select name="category_id" class="form-control" id="category" value="{{old('category_id')}}">
                            <option></option>
                            @foreach($categories as $category)
                                @if($product->category_id == $category->id)
                                    <option selected value="{{$category->id}}">{{$category->name}}</option>
                                @else
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endif
                                
                            @endforeach
                        </select>
                        @error('category_id')
                            <small class="form-text text-muted">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="title">Title</label>
                        <input type="text" name="title" value="{{$product->title}}" class="form-control" id="title" placeholder="Title">
                        @error('title')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" id="description" rows="10">{{$product->description}}</textarea>
                        @error('description')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="file">Image</label>
                        @if($product->image)
                            <div class="image">
                                <a href="javascript:void(0);" class="remove-single-image"></a>
                                <img class="single-image" src="{{url('storage/' . $product->image)}}" data-id="{{$product->id}}">
                            </div>
                        @endif
                        <input type="file" name="image" value="{{ $product->image }}" class="form-control" id="file">
                        @error('image')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" value="{{$product->phone}}" class="form-control" id="phone" placeholder="Phone">
                        @error('phone')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" value="{{$product->slug}}" class="form-control" id="slug" readonly="">
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
