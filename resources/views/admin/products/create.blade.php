@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4 mb-4">Create New Product</h1>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Product
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{route('store_product')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-3">
                        <label for="category_id">Category</label>
                        <select name="category_id" class="form-control" id="category_id" value="{{old('category_id')}}">
                            <option></option>
                            @foreach($categories as $category)
                                @if($category->id == old('category_id'))
                                    <option selected="" value="{{$category->id}}">{{$category->name}}</option>
                                @else
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('category_id')
                            <small class="form-text text-muted">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="title">Title</label>
                        <input type="text" name="title" value="{{old('title')}}" class="form-control" id="title" placeholder="Title">
                        @error('title')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" id="description" rows="10">{{old('description')}}</textarea>
                        @error('description')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="file">Image</label>
                        <input type="file" name="image" class="form-control" id="file">
                        @error('image')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" value="{{old('phone')}}" class="form-control" id="phone" placeholder="Phone">
                        @error('phone')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
