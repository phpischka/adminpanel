@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4 mb-4">Users</h1>

        <div class="row">
            <div class="col-xl-2">
                <div class="card mb-4">
                    <a href="{{route('create_user')}}" class="btn btn-success" role="button" aria-pressed="true">Add</a>
                </div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                User list
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                 <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td> 
                                    <div class="col-md-12">
                                        <a href="{{url('admin/users/' . $user->id . '/edit')}}" class="btn btn-primary btn-block mb-1 btn-sm" role="button" aria-pressed="true">Edit</a>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="{{url('admin/users/' . $user->id . '/delete')}}" class="btn btn-danger btn-block btn-sm" role="button" aria-pressed="true">Delete</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection
