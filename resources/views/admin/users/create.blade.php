@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4 mb-4">Create New User</h1>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                User
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{route('store_user')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-3">
                        <label for="role_id">Role</label>
                        <select name="role_ids[]" class="form-control" id="role_id" value="{{old('role_id')}}" multiple="multiple">
                            <option></option>
                            @foreach($roles as $role)
                                @php
                                    $oldRoleIds = old('role_ids') ?? [];
                                @endphp
                                @if(in_array($role->id, $oldRoleIds))
                                    <option selected="" value="{{$role->id}}">{{$role->name}}</option>
                                @else
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('role_ids')
                            <small class="form-text text-muted">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" placeholder="Name">
                        @error('name')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{old('email')}}" class="form-control" id="email" placeholder="Email">
                        @error('email')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        @error('password')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="password_confirmation">Password Confirmation</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirmation">
                        @error('password_confirmation')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
