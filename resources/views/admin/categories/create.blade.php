@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4 mb-4">Create New Category</h1>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Product
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{route('store_category')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-3">
                        <label for="parent_id">Parent Category</label>
                        <select name="parent_id" class="form-control" id="parent_id" value="{{old('parent_id')}}">
                            <option></option>
                            @foreach($categories as $category)
                                @if($category->id == old('parent_id'))
                                    <option selected="" value="{{$category->id}}">{{$category->name}}</option>
                                @else
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('parent_id')
                            <small class="form-text text-muted">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" placeholder="Title">
                        @error('name')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" value="{{old('slug')}}" class="form-control" id="slug" placeholder="Title">
                        @error('slug')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="status">Status</label>
                        <select name="status" class="form-control" id="status" value="{{old('status')}}">
                            @foreach($statuses as $status)
                                <option value="{{$status}}">{{$status}}</option>
                            @endforeach
                        </select>
                        @error('status')
                            <small class="form-text text-muted">{{ $message }}</small>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
