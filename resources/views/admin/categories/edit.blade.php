@extends('layouts.admin.app')

@section('content')
<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4 mb-4">Edit Category</h1>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Category
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{url('admin/categories/' . $category->id . '/update')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-3">
                        <label for="parent_id">Parent Category</label>
                        <select name="parent_id" class="form-control" id="parent_id">
                            <option></option>
                            @foreach($categories as $itemCategory)
                                @if($category->parent_id == $itemCategory->id)
                                    <option selected value="{{$itemCategory->id}}">{{$itemCategory->name}}</option>
                                @else
                                    <option value="{{$itemCategory->id}}">{{$itemCategory->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('parent_id')
                            <small class="form-text text-muted">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{$category->name}}" class="form-control" id="name" placeholder="Title">
                        @error('name')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" value="{{$category->slug}}" class="form-control" id="slug" placeholder="Title">
                        @error('slug')                            
                            <small class="form-text text-muted">{{ $message }}</small>                        
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="status">Status</label>
                        <select name="status" class="form-control" id="status" value="{{$category->status}}">
                            @foreach($statuses as $status)
                                @if($category->status == $status)
                                    <option selected value="{{$status}}">{{$status}}</option>
                                @else
                                    <option value="{{$status}}">{{$status}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('status')
                            <small class="form-text text-muted">{{ $message }}</small>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
