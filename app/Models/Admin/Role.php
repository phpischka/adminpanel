<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    
    /**
   * Пользователи, принадлежащие роли.
   */
  public function users()
  {
    return $this->belongsToMany('App\Models\User');
  }
  
    /**
   * разрешения, принадлежащие роли.
   */
  public function permissions()
  {
    return $this->belongsToMany(Permission::class,'roles_permissions');
  }
}
