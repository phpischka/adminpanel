<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    
    /**
     * Statuses.
     */
    public const STATUS_ACTIVE = 'ACTIVE';
    public const STATUS_INACTIVE = 'INACTIVE';
    
    /**
     * List of statuses.
     *
     * @var array
     */
    public static $statuses = [self::STATUS_ACTIVE, self::STATUS_INACTIVE];
    
    public function parent() {
        return $this->belongsTo(self::class, 'parent_id');
    }
    
    public function children() {
        return $this->hasMany(self::class, 'parent_id');
    }
    
    public function products() {
        return $this->hasMany(Product::class);
    }
}
