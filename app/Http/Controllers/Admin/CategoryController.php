<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('parent')->get();
        
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', Category::STATUS_ACTIVE)->get();
        
        $statuses = Category::$statuses;
                
        return view('admin.categories.create', compact('categories', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'slug' => 'required'
        ]);
        try {
            $category = new Category();
            $category->parent_id = $request->input('parent_id');
            $category->name = $request->input('name');
            $category->slug = $request->input('slug');
            $category->status = $request->input('status');
            $category->save();
        } catch (\Exception $e) {
            return back()->with('status', $e);
        }
        
        return redirect()->route('categories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $category = Category::where('id', $id)->first();
        
        $categories = Category::where('status', Category::STATUS_ACTIVE)
                ->where('id', '!=', $id)->get();
        
        $statuses = Category::$statuses;
        
        if (!$category) {    
            return abort(404);
        }
        return view('admin.categories.edit', compact('category', 'categories', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'slug' => 'required'
        ]);
        try {
            $category = Category::where('id', $id)->first();
            $category->parent_id = $request->input('parent_id');
            $category->name = $request->input('name');
            $category->slug = $request->input('slug');
            $category->status = $request->input('status');
            $category->save();
        } catch (\Exception $e) {
            return back()->with('status', $e);
        }
        
        return redirect()->route('categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        
        if ($category) {
          $category->delete();
        }
        return redirect()->route('categories');
    }
}
