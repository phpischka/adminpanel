<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Image;
use App\Models\Admin\Product;

class ImageController extends Controller
{
    public static function store($image) 
    {
        
        $input['imagename'] = time().'.'.$image->extension();

        $destinationPath = public_path('/storage/products');

        $img = Image::make($image->path());

        $img->fit(200)->save($destinationPath.'/'.$input['imagename']);
        
        return 'products/'.$input['imagename'];
        
    }
    public static function destroy($idProduct) 
    {
       
       $product = Product::where('id', $idProduct)->first();
       $product->image = '';
       $product->update();
       
       return 200;
    }
}
