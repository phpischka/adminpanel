<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Product;
use App\Models\Admin\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('category')->get();
        
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', Product::STATUS_ACTIVE)->get();
        
        return view('admin.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'title' => 'required|string|max:255',
            'description' => 'required',
            'phone' => 'required|string|max:255',
        ]);
        try {
            $image = $request->file('image');
            
            if ($image) {
                $imagePath = ImageController::store($image);
            }
            
            $product = new Product();
            $product->category_id = $request->input('category_id');
            $product->title = $request->input('title');
            $product->description = $request->input('description');
            $product->image = $imagePath ?? '';
            $product->phone = $request->input('phone');
            $product->slug = Product::slugGenerate($request->input('title'));
            $product->status = Product::STATUS_INACTIVE;
            $product->save();
        } catch (\Exception $e) {
            return back()->with('status', $e);
        }
        
        return redirect()->route('products');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('status', Product::STATUS_ACTIVE)->get();
        
        $product = Product::where('id', $id)->first();
        
        if (!$product) {    
            return abort(404);
        }
        return view('admin.products.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_id' => 'required',
            'title' => 'required|string|max:255',
            'description' => 'required',
            'phone' => 'required|string|max:255',
        ]);
        try {
            
            $product = Product::where('id', $id)->first();
            $product->category_id = $request->input('category_id');
            $product->title = $request->input('title');
            $product->description = $request->input('description');
            
            if ($request->file('image')) {
                $imagePath = ImageController::store($request->file('image'));
                $product->image = $imagePath;
            }

            $product->phone = $request->input('phone');
            $product->slug = $request->input('slug');
            $product->save();
        } catch (\Exception $e) {
            return back()->with('status', $e);
        }
        
        return redirect()->route('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        
        if ($product) {
          $product->delete();
        }
        return redirect()->route('products');
    }
}
