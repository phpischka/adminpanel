<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        
        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:20',
            'slug' => 'required|string|max:20',
        ]);
        
        try {
            
            $role = new Role();
            $role->name = $request->input('name');
            $role->slug = $request->input('slug');
            $role->save();
            
        } catch (\Exception $e) {
            return back()->with('status', $e);
        }
        
        return redirect()->route('roles');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $role = Role::where('id', $id)->first();
        
        if (!$role) {    
            return abort(404);
        }
        
        return view('admin.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'name' => 'required|string|max:20',
            'slug' => 'required|string|max:20',
        ]);
        
        try {
            
            $role = Role::where('id', $id)->first();
            $role->name = $request->input('name');
            $role->slug = $request->input('slug');
            $role->save();
            
        } catch (\Exception $e) {
            return back()->with('status', $e);
        }
        
        return redirect()->route('roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        
        if ($role) {
          $role->delete();
        }
        return redirect()->route('roles');
    }
}
